# relay_rs #

Rust-based port forwarding, for securely forwarding intranet ports through the server.

### simple proxy ###

> [example](examples/proxy.rs)

```rust
use relay::*;
use tokio::io::AsyncWriteExt;

#[tokio::main]
async fn main() -> Res<()> {

    // configure[ssh proxy]
    let address = "127.0.0.1:10022";
    let proxy_address = "127.0.0.1:22";

    // network
    let listener = TcpListener::bind(address,None).await?;

    loop {
        match listener.accept().await {
            Ok((inbound, addr)) => {
                println!("new client: {:?}", addr);

                let outbound= match TcpStream::connect_addr(proxy_address,None).await{
                    Ok(res) => res,
                    Err(e) => {
                        eprintln!("{:?}",e);
                        continue;
                    }
                };

                tokio::spawn(async move{
                    if let Err(e) = proxy(inbound,outbound).await {
                        eprintln!("{:?}",e)
                    }
                });

            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


async fn proxy(mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();

    tokio::try_join!(async {
        copy(&mut ri,&mut wo,CopyMode::Default).await?;
        wo.shutdown().await
    },async {
        copy(&mut ro,&mut wi,CopyMode::Default).await?;
        wi.shutdown().await
    })?;

    Ok(())
}
```

### Safe Proxy ###

#### server  ####

> [example](examples/copy-server.rs)

```rust
use relay::*;
use tokio::io::AsyncWriteExt;


#[tokio::main]
async fn main() -> Res<()> {

    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:20022";
    let proxy_address = "127.0.0.1:22"; // local ssh address

    // network
    let listener = TcpListener::bind(address,None).await?;

    loop {
        match listener.accept().await {
            Ok((socket, addr)) => {
                println!("new client: {:?}", addr);

                let proxy_socket = TcpStream::connect_addr(proxy_address,None).await?;
                let cipher_other = cipher.clone();

                tokio::spawn(async move {
                    if let Err(e) = proxy(cipher_other,socket,proxy_socket).await{
                        eprintln!("{:?}",e);
                    }
                });
            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


pub async fn proxy(cipher:CipherHandler,mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let (mut ro, mut wo) = outbound.split();

    let cipher_other = cipher.clone();
    tokio::try_join!(
        async move {
            copy(&mut ri,&mut wo,CopyMode::Decrypt(cipher_other)).await?;
            wo.shutdown().await
        },
        async move {
            copy(&mut ro,&mut wi,CopyMode::Encrypt(cipher)).await?;
            wi.shutdown().await
        }
    )?;
    Ok(())
}
```

#### client  ####

> [example](examples/copy-client.rs)

```rust
use relay::*;
use tokio::io::AsyncWriteExt;


#[tokio::main]
async fn main() -> Res<()> {

    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:10022";
    let proxy_address = "127.0.0.1:20022";

    // network
    let listener = TcpListener::bind(address,None).await?;

    loop {
        match listener.accept().await {
            Ok((socket, addr)) => {
                println!("new client: {:?}", addr);

                let proxy_socket = TcpStream::connect_addr(proxy_address,None).await?;
                let cipher_other = cipher.clone();

                tokio::spawn(async move {
                    if let Err(e) = proxy(cipher_other,socket,proxy_socket).await{
                        eprintln!("{:?}",e);
                    }
                });
            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


pub async fn proxy(cipher:CipherHandler,mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let (mut ro, mut wo) = outbound.split();

    let cipher_other = cipher.clone();
    tokio::try_join!(
        async move {
            copy(&mut ri,&mut wo,CopyMode::Encrypt(cipher_other)).await?;
            wo.shutdown().await
        },
        async move {
            copy(&mut ro,&mut wi,CopyMode::Decrypt(cipher)).await?;
            wi.shutdown().await
        }
    )?;
    Ok(())
}
```