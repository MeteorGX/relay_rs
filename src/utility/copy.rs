use tokio::io::{AsyncRead, AsyncWrite};
use tokio::io::AsyncWriteExt;
use tokio::net::tcp::{ReadHalf, WriteHalf};
use tokio::io::AsyncReadExt;


pub struct CopyStream;
pub struct CopyQueueStream;


impl CopyStream{

    pub async fn from<R,W>(reader:&mut R,writer:&mut W)->crate::Res<u64>
        where R:AsyncRead + Unpin + ?Sized,W:AsyncWrite + Unpin + ?Sized{
        Ok(tokio::io::copy(reader,writer).await?)
    }

    async fn copy(mut redaer:ReadHalf<'_>,mut writer:WriteHalf<'_>)->crate::Res<u64>{
        let sz = Self::from(&mut redaer,&mut writer).await?;
        writer.shutdown().await?;
        Ok(sz)
    }

    pub async fn from_stream(mut inbound:crate::TcpStream,mut outbound:crate::TcpStream)->crate::Res<(u64,u64)>{
        let (ri,wi) = inbound.split();
        let (ro,wo) = outbound.split();
        Ok(tokio::try_join!(
            Self::copy(ri,wo),
            Self::copy(ro,wi)
        )?)
    }

    pub async fn from_tokio_stream(mut inbound:tokio::net::TcpStream,mut outbound:tokio::net::TcpStream)->crate::Res<(u64,u64)>{
        let (ri,wi) = inbound.split();
        let (ro,wo) = outbound.split();
        Ok(tokio::try_join!(
            Self::copy(ri,wo),
            Self::copy(ro,wi)
        )?)
    }
}


impl CopyQueueStream{
    pub async fn from_stream(mut inbound:crate::TcpStream,mut outbound:crate::TcpStream,capacity:usize)->crate::Res<()>{
        let (mut ri,mut wi) = inbound.split();
        let (mut ro,mut wo) = outbound.split();
        let mut buffer_inbound = vec![0u8;capacity];
        let mut buffer_outbound = vec![0u8;capacity];

        loop {
            tokio::select! {
                res_inbound = ri.read(&mut buffer_inbound) => match res_inbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        wo.write_all(&buffer_inbound[0..sz]).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                },
                res_outbound = ro.read(&mut buffer_outbound) => match res_outbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        wi.write_all(&buffer_outbound[0..sz]).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                }
            };
        }
    }

    pub async fn from_tokio_stream(inbound:tokio::net::TcpStream,outbound:tokio::net::TcpStream,capacity:usize)->crate::Res<()>{
        Ok(Self::from_stream(
            crate::TcpStream::from_tokio(inbound).await?,
            crate::TcpStream::from_tokio(outbound).await?,
            capacity
        ).await?)
    }
}
