use tokio::io::{AsyncRead, AsyncReadExt};

pub struct ByteBuffer{
    data:Vec<u8>,
}



impl ByteBuffer{
    pub fn new()->Self{
        Self{ data: vec![0u8;4 * 1024] }
    }

    pub fn with_capacity(sz:usize)->Self{
        Self{ data: vec![0u8;sz]}
    }

    pub async fn read<R>(&mut self, reader:&mut R) ->crate::Res<&[u8]>
        where R:AsyncRead+Unpin+?Sized{
        let sz = match reader.read(self.data.as_mut_slice()).await{
            Ok(res) if res == 0 => { return Ok(b"") },
            Ok(res) => res,
            Err(e) => { return Err(e.into()) }
        };
        Ok( &self.data[0..sz] )
    }

}
