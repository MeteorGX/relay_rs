
macro_rules! import_error{
   ($e:path) => {
       impl From<$e> for Error{
           fn from(src:$e)->Self{src.to_string().into()}
       }
   };
}

mod inner {
    pub use std::fmt::{Display,Formatter,Result};
}


/// generic error box types.
///
/// declare the Box<Error> container to place the error object.
///
pub type EBox = Box<dyn std::error::Error+Sync+Send>;


/// generic custom error types
///
///
#[derive(Debug)]
pub enum Error{
    CipherMethodUnSupport,
    CipherNonFound,
    FailedReadCipherIV,
    ConnectionReset,
    Incomplete,
    Other(EBox)
}



#[allow(unreachable_patterns)]
impl inner::Display for Error{
    fn fmt(&self, f: &mut inner::Formatter<'_>) -> inner::Result {
        match self{
            Error::CipherMethodUnSupport => "openssl method unsupport".fmt(f),
            Error::FailedReadCipherIV => "failed by read cipher iv".fmt(f),
            Error::CipherNonFound => "failed by get cipher".fmt(f),
            Error::ConnectionReset => "connection reset by peer".fmt(f),
            Error::Incomplete => "not enough data".fmt(f),
            Error::Other(e) => e.fmt(f)
        }
    }
}

/// extended inheritance system error
///
impl std::error::Error for Error{ }


impl From<EBox> for Error{
    fn from(src:EBox)->Self{ Error::Other(src) }
}

impl From<String> for Error{
    fn from(src:String)->Self{ Error::Other(src.into()) }
}

impl From<&str> for Error{
    fn from(src:&str)->Self{ Error::Other(src.to_string().into()) }
}

impl From<Error> for std::io::Error{
    fn from(src:Error)->Self{ std::io::Error::new(std::io::ErrorKind::Other,src.to_string()) }
}

import_error!(std::io::Error);
import_error!(openssl::error::ErrorStack);
import_error!(std::str::Utf8Error);
import_error!(std::num::ParseIntError);
import_error!(std::net::AddrParseError);

/// declare the result<T,error> alias
///
/// # Examples
///
/// ```edition2018
/// fn main()->relay::Res<()>{
///     println!("hello.world!");
///     Ok(())
/// }
/// ```
pub type Res<T> = Result<T,Error>;
