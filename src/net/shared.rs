use std::collections::HashMap;
use std::net::SocketAddr;

pub type ByteSender = tokio::sync::mpsc::UnboundedSender<Vec<u8>>;
pub type ByteReceiver = tokio::sync::mpsc::UnboundedReceiver<Vec<u8>>;


pub struct ByteShared{
    peers:HashMap<SocketAddr,ByteSender>
}

impl ByteShared{
    pub fn new()->Self{
        Self{peers:HashMap::new()}
    }

    async fn broadcast(&mut self,sender:SocketAddr,message:&[u8]){
        for peer in self.peers.iter_mut() {
            if *peer.0 != sender {
                let _ = peer.1.send(message.into());
            }
        }
    }
}
