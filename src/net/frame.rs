use tokio::io::{AsyncWrite, AsyncWriteExt, AsyncRead, AsyncReadExt};
use bytes::{BytesMut, BufMut, Buf};
use std::io::Cursor;
use std::net::{Ipv4Addr, Ipv6Addr};

pub mod inner{
    use bytes::Buf;

    pub const DEFAULT_BUFFER_LENGTH:usize = 16 * 1024;// 6KB/frame


    pub(crate) const DELIMITER_LEFT:u8 = b'\r';
    pub(crate) const DELIMITER_RIGHT:u8 = b'\n';

    pub(crate) const DELIMITER_BYTES:u8 = b'B';
    pub(crate) const DELIMITER_STRING:u8 = b'S';
    pub(crate) const DELIMITER_NUMBER:u8 = b'N';
    pub(crate) const DELIMITER_TIMESTAMP:u8 = b'T';
    pub(crate) const DELIMITER_IPV4:u8 = b'4';
    pub(crate) const DELIMITER_IPV6:u8 = b'6';
    pub(crate) const DELIMITER_HOSTNAME:u8 = b'H';
    pub(crate) const DELIMITER_UNKNOWN:u8 = 0xff;


    /// Find a U8
    #[inline]
    pub(crate) fn get_u8(src: &mut super::Cursor<&[u8]>) -> crate::Res<u8> {
        if !src.has_remaining() {
            return Err(crate::Error::Incomplete);
        }
        Ok(src.get_u8())
    }

    /// Find a line
    #[inline]
    pub(crate) fn get_line<'a>(src: &mut super::Cursor<&'a [u8]>) -> crate::Res<&'a [u8]>{
        // Scan the bytes directly
        let start = src.position() as usize;
        // Scan to the second to last byte
        let end = src.get_ref().len() - 1;

        for i in start..end {
            if src.get_ref()[i] == DELIMITER_LEFT && src.get_ref()[i + 1] == DELIMITER_RIGHT {
                // We found a line, update the position to be *after* the \n
                src.set_position((i + 2) as u64);

                // Return the line
                return Ok(&src.get_ref()[start..i]);
            }
        }
        Err(crate::Error::Incomplete)
    }


    /// Read a new-line terminated number
    #[inline]
    pub(crate) fn get_number(src: &mut super::Cursor<&[u8]>) -> crate::Res<u64> {
        use atoi::atoi;
        let line = get_line(src)?;
        atoi::<u64>(line).ok_or_else(|| "protocol error; invalid frame format".into())
    }
}


#[derive(Debug,Clone)]
pub enum Frame{
    Bytes(Vec<u8>),
    String(String),
    Number(u64),
    Timestamp(u64),
    IPv4(Ipv4Addr,u16),
    IPv6(Ipv6Addr,u16),
    Hostname(String,u16),
    Unknown
}


///
///
impl From<&Frame> for u8{
    fn from(src: &Frame) -> Self {
        match src {
            Frame::Bytes(_) => inner::DELIMITER_BYTES,
            Frame::String(_) => inner::DELIMITER_STRING,
            Frame::Number(_) => inner::DELIMITER_NUMBER,
            Frame::Timestamp(_) => inner::DELIMITER_TIMESTAMP,
            Frame::IPv4(_,_) => inner::DELIMITER_IPV4,
            Frame::IPv6(_,_) => inner::DELIMITER_IPV6,
            Frame::Hostname(_,_) => inner::DELIMITER_HOSTNAME,
            _ => inner::DELIMITER_UNKNOWN
        }
    }
}

impl From<u8> for Frame{
    fn from(src: u8) -> Self { src.into() }
}

impl From<&mut Frame> for u8{
    fn from(src:&mut Frame) -> Self {
        match src {
            Frame::Bytes(_) => inner::DELIMITER_BYTES,
            Frame::String(_) => inner::DELIMITER_STRING,
            Frame::Number(_) => inner::DELIMITER_NUMBER,
            Frame::Timestamp(_) => inner::DELIMITER_TIMESTAMP,
            Frame::IPv4(_,_) => inner::DELIMITER_IPV4,
            Frame::IPv6(_,_) => inner::DELIMITER_IPV6,
            Frame::Hostname(_,_) => inner::DELIMITER_HOSTNAME,
            _ => inner::DELIMITER_UNKNOWN
        }
    }
}



impl Frame {
    pub fn match_frame(&mut self)->crate::Res<Vec<u8>>{
        let res = match self {
            Frame::Bytes(ref res) => res.to_vec(),
            Frame::String(ref res) => res.as_bytes().to_vec(),
            Frame::Number(ref res) => res.to_string().as_bytes().to_vec(),
            Frame::Timestamp(res) => res.to_string().as_bytes().to_vec(),
            Frame::IPv4(address,port) => {
                let mut res = BytesMut::new();
                res.put_slice(&address.octets());
                res.put_u16(*port);
                res.to_vec()
            },
            Frame::IPv6(address,port) => {
                let mut res = BytesMut::new();
                res.put_slice(&address.octets());
                res.put_u16(*port);
                res.to_vec()
            },
            Frame::Hostname(address,port) => {
                let mut res = BytesMut::new();
                let address_bytes = address.as_bytes();
                res.put_u8(address_bytes.len() as u8);
                res.put_slice(address.as_bytes());
                res.put_u16(*port);
                res.to_vec()
            },
            Frame::Unknown => { return Err(crate::Error::Incomplete); }
        };
        Ok(res)
    }


    pub async fn write<W>(&mut self,writer:&mut W)->crate::Res<()>
        where W:AsyncWrite + Unpin + ?Sized{

        let frame_type:u8 = self.into();
        let frame_ctx = self.match_frame()?;
        let frame_ctx = frame_ctx.as_slice();
        let frame_length = frame_ctx.len() as u32;

        let mut buffer = BytesMut::with_capacity(
            std::mem::size_of::<u32>() + std::mem::size_of::<u8>() + frame_ctx.len() + 2
        );
        buffer.put_u32(frame_length + std::mem::size_of::<u8>() as u32 + 2);// [frame length]
        buffer.put_u8(frame_type);// [frame type]
        buffer.put_slice(frame_ctx);
        buffer.put_slice(&[inner::DELIMITER_LEFT,inner::DELIMITER_RIGHT]);
        Ok(writer.write_all(buffer.as_ref()).await?)
    }


    pub async fn read<R>(reader: &mut R) ->crate::Res<Option<Self>>
        where R:AsyncRead + Unpin + Sized{
        let frame_length = reader.read_u32().await? as usize;
        let mut buffer = vec![0u8;frame_length];
        if reader.read_exact(buffer.as_mut_slice()).await? != frame_length{
            return Err(crate::Error::ConnectionReset);
        }
        let mut bytes = BytesMut::from(buffer.as_slice());
        Ok(Self::parse_frame(&mut bytes)?)
    }

    pub fn parse_frame(buffer:&mut BytesMut)->crate::Res<Option<Self>>{
        let mut buf = Cursor::new(&buffer[..]);
        match Frame::check(&mut buf){
            Ok(_) =>{
                // reset position
                let sz = buf.position() as usize;
                buf.set_position(0);
                let frame = Frame::parse(&mut buf)?;
                buffer.advance(sz);
                Ok(Some(frame))
            }
            Err(crate::Error::Incomplete) => Ok(None),
            Err(e) => Err(e.into())
        }
    }

    pub fn check(src: &mut Cursor<&[u8]>)->crate::Res<()>{
        match inner::get_u8(src)? {
            inner::DELIMITER_BYTES => { // bytes
                inner::get_line(src)?;
                Ok(())
            },
            inner::DELIMITER_STRING => { // string
                inner::get_line(src)?;
                Ok(())
            },
            inner::DELIMITER_NUMBER => { // number
                inner::get_number(src)?;
                Ok(())
            },
            inner::DELIMITER_TIMESTAMP =>{ // timestamp
                inner::get_number(src)?;
                Ok(())
            },
            inner::DELIMITER_IPV4 =>{ // ipv4
                inner::get_line(src)?;
                Ok(())
            },
            inner::DELIMITER_IPV6 =>{ // ipv6
                inner::get_line(src)?;
                Ok(())
            },
            inner::DELIMITER_HOSTNAME =>{ // hostname
                inner::get_line(src)?;
                Ok(())
            },
            actual => Err(format!("protocol error; invalid frame type byte `{}`", actual).into()),
        }
    }

    pub fn parse(src: &mut Cursor<&[u8]>)->crate::Res<Self>{
        match inner::get_u8(src)? {
            inner::DELIMITER_BYTES => { // bytes
                let data = inner::get_line(src)?.to_vec();
                Ok(Self::Bytes(data))
            },
            inner::DELIMITER_STRING => { // string
                let data = inner::get_line(src)?.to_vec();
                Ok(Self::String(std::str::from_utf8(data.as_slice())?.to_string()))
            },
            inner::DELIMITER_NUMBER => { // number
                let number = inner::get_number(src)?;
                Ok(Self::Number(number))
            },
            inner::DELIMITER_TIMESTAMP =>{ // timestamp
                let timestamp = inner::get_number(src)?;
                Ok(Self::Timestamp(timestamp))
            },
            inner::DELIMITER_IPV4 =>{ // ipv4
                let buf = inner::get_line(src)?;
                if buf.len() != 6 {
                    return Err(crate::Error::Incomplete);
                }
                let address = Ipv4Addr::new(buf[0],buf[1],buf[2],buf[3]);
                let port = ((buf[4] as u16) << 8) | (buf[5] as u16);
                Ok(Self::IPv4(address,port))
            },
            inner::DELIMITER_IPV6 =>{ // ipv6
                let buf = inner::get_line(src)?;
                if buf.len() != 18 {
                    return Err(crate::Error::Incomplete);
                }
                let a = ((buf[0] as u16) << 8) | (buf[1] as u16);
                let b = ((buf[2] as u16) << 8) | (buf[3] as u16);
                let c = ((buf[4] as u16) << 8) | (buf[5] as u16);
                let d = ((buf[6] as u16) << 8) | (buf[7] as u16);
                let e = ((buf[8] as u16) << 8) | (buf[9] as u16);
                let f = ((buf[10] as u16) << 8) | (buf[11] as u16);
                let g = ((buf[12] as u16) << 8) | (buf[13] as u16);
                let h = ((buf[14] as u16) << 8) | (buf[15] as u16);

                let address = Ipv6Addr::new(a, b, c, d, e, f, g, h);
                let port = ((buf[16] as u16) << 8) | (buf[17] as u16);
                Ok(Self::IPv6(address,port))
            },
            inner::DELIMITER_HOSTNAME =>{ // hostname
                let buf = inner::get_line(src)?;
                let address_len = buf[0] as usize;
                if buf.len() != address_len + 3 {
                    return Err(crate::Error::Incomplete);
                }

                let pos = buf.len() - 2;
                let address = std::str::from_utf8(&buf[1..buf.len() - 2])?;
                let port = ((buf[pos] as u16) << 8) | (buf[pos + 1] as u16);
                Ok(Self::Hostname(address.to_string(),port))
            },
            _ => unimplemented!(),
        }
    }
}