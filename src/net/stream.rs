use crate::CipherHandler;
use tokio::io::{AsyncReadExt, AsyncWriteExt, ReadBuf};
use tokio::net::ToSocketAddrs;
use tokio::net::tcp::{WriteHalf,ReadHalf};
use std::task::{Context, Poll};
use std::pin::Pin;
use std::io::Error;


#[derive(Debug)]
pub struct TcpStream{
    cipher:Option<CipherHandler>,
    iv:Option<Vec<u8>>,
    handler:tokio::net::TcpStream,
}


impl TcpStream{


    pub async fn new(mut stream:tokio::net::TcpStream,cipher:Option<CipherHandler>)->crate::Res<Self>{
        match cipher{
            None => {
                Ok(Self{
                    cipher:None,
                    iv:None,
                    handler:stream,
                })
            }
            Some(cipher) => {
                // create iv buffer
                let iv_len = cipher.get_iv_len();
                let mut iv = vec![0u8;iv_len];

                // rc4.... pass
                if iv_len > 0 {
                    // read iv
                    stream.read(iv.as_mut_slice()).await?;
                }
                Ok(Self{
                    cipher:Some(cipher),
                    iv:Some(iv),
                    handler:stream,
                })
            }
        }
    }

    pub async fn from_tokio(handler:tokio::net::TcpStream)->crate::Res<Self>{
        Ok(Self{
            cipher: None,
            iv: None,
            handler,
        })
    }

    pub async fn connect_with_cipher<A: ToSocketAddrs>(addr: A,cipher:Option<CipherHandler>) -> crate::Res<Self> {
        match cipher {
            None => {
                Ok(Self{
                    cipher:None,
                    iv:None,
                    handler:tokio::net::TcpStream::connect(addr).await?,
                })
            }
            Some(cipher) => {
                // create iv buffer
                let iv = cipher.generate_iv();

                // write iv
                let mut handler = tokio::net::TcpStream::connect(addr).await?;
                handler.write_all(iv.as_slice()).await?;

                Ok(Self{
                    cipher:Some(cipher),
                    iv:Some(iv),
                    handler,
                })
            }
        }
    }

    pub async fn connect<A: ToSocketAddrs>(addr: A) -> crate::Res<Self> {
        Ok(Self::connect_with_cipher(addr,None).await?)
    }

    pub async fn read(&mut self, buff:&mut [u8]) ->crate::Res<usize>{
        match self.cipher{
            None => { Ok(self.handler.read(buff).await?) }
            Some(ref cipher) => {
                let iv = match self.iv {
                    Some(ref res) => res,
                    None => { return Err(crate::Error::CipherNonFound); }
                };

                // read cipher
                let sz = self.handler.read(buff).await?;
                let data = &buff[0..sz];
                let decrypt = cipher.decrypt_with_iv(data,iv.as_slice())?;

                // reset buffer
                for i in 0..decrypt.len(){ buff[i] = decrypt[i]; }
                Ok(sz)
            }
        }
    }

    pub async fn read_exact(&mut self, buff:&mut [u8]) ->crate::Res<usize>{
        match self.cipher{
            None => { Ok(self.handler.read_exact(buff).await?) }
            Some(ref cipher) => {
                let iv = match self.iv {
                    Some(ref res) => res,
                    None => { return Err(crate::Error::CipherNonFound); }
                };

                let sz = self.handler.read_exact(buff).await?;
                let data = &buff[0..sz];
                let decrypt = cipher.decrypt_with_iv(data,iv.as_slice())?;

                // reset buffer
                for i in 0..decrypt.len(){ buff[i] = decrypt[i]; }
                Ok(sz)
            }
        }
    }

    pub async fn write(&mut self, buff:&[u8]) ->crate::Res<usize>{
        match self.cipher {
            None => { Ok(self.handler.write(buff).await?) }
            Some(ref cipher) => {
                let iv = match self.iv {
                    Some(ref res) => res,
                    None => { return Err(crate::Error::CipherNonFound); }
                };

                let encrypt = cipher.encrypt_with_iv(buff,iv.as_slice())?;
                Ok(self.handler.write(encrypt.as_slice()).await?)
            }
        }
    }

    pub async fn write_all(&mut self, buff:&[u8]) ->crate::Res<()>{
        match self.cipher {
            None => { Ok(self.handler.write_all(buff).await?) }
            Some(ref cipher) => {
                let iv = match self.iv {
                    Some(ref res) => res,
                    None => { return Err(crate::Error::CipherNonFound); }
                };

                let encrypt = cipher.encrypt_with_iv(buff,iv.as_slice())?;
                Ok(self.handler.write_all(encrypt.as_slice()).await?)
            }
        }
    }

    pub fn split(&mut self) -> (ReadHalf, WriteHalf) {
        self.handler.split()
    }


    #[allow(unused_must_use)]
    pub async fn copy(&mut self, other: &mut Self) ->crate::Res<()>{
        let (mut ri,mut wi) = self.split();
        let (mut ro,mut wo) = other.split();
        tokio::try_join!(async move {
            tokio::io::copy(&mut ri,&mut wo).await?;
            wo.shutdown().await
        },async move {
            tokio::io::copy(&mut ro,&mut wi).await?;
            wi.shutdown().await
        });
        Ok(())
    }

}


impl tokio::io::AsyncWrite for TcpStream{
    fn poll_write(mut self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &[u8]) -> Poll<Result<usize, Error>> {
        let me = &mut *self;
        Pin::new(&mut me.handler).poll_write(cx,buf)
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        let me = &mut *self;
        Pin::new(&mut me.handler).poll_flush(cx)
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        let me = &mut *self;
        Pin::new(&mut me.handler).poll_shutdown(cx)
    }
}


impl tokio::io::AsyncRead for TcpStream{
    fn poll_read(mut self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &mut ReadBuf<'_>) -> Poll<tokio::io::Result<()>> {
        let me = &mut *self;
        Pin::new(&mut me.handler).poll_read(cx,buf)
    }
}