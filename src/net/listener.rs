use crate::CipherHandler;
use crate::TcpStream;
use tokio::net::ToSocketAddrs;
use std::net::SocketAddr;

#[derive(Debug)]
pub struct TcpListener{
    cipher:Option<CipherHandler>,
    handler:tokio::net::TcpListener
}

impl TcpListener{
    pub async fn bind_with_cipher<A: ToSocketAddrs>(addr: A,cipher:Option<CipherHandler>) ->crate::Res<Self>{
        Ok(Self{
            cipher,
            handler: tokio::net::TcpListener::bind(addr).await?,
        })
    }


    pub async fn bind<A: ToSocketAddrs>(addr: A) ->crate::Res<Self>{
        Ok(Self::bind_with_cipher(addr,None).await?)
    }

    pub async fn accept(&self) -> crate::Res<(TcpStream,SocketAddr)>{
        let (stream,sockaddr) = self.handler.accept().await?;
        Ok((TcpStream::new(stream,if self.cipher.is_some() { self.cipher.clone() } else { None }).await?,sockaddr))
    }
}