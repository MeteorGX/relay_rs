use relay::*;
use tokio::io::{AsyncWriteExt, AsyncReadExt};
use std::net::Ipv4Addr;
use std::str::FromStr;


#[tokio::main]
async fn main() -> Res<()> {


    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:10022";
    let proxy_address = "127.0.0.1:20022";

    // network
    let listener = TcpListener::bind(address).await?;
    loop {
        match listener.accept().await {
            Ok((inbound, addr)) => {
                println!("new client: {:?}", addr);
                let mut outbound = TcpStream::connect(proxy_address).await?;
                let proxy_other = cipher.clone();
                tokio::spawn(async move{
                    let iv = match proxy_other.read_iv(&mut outbound).await {
                        Ok(res) => res,
                        Err(e) => {
                            eprint!("{:?}",e);
                            return ;
                        }
                    };

                    if let Err(e) = proxy(inbound,outbound,proxy_other,iv).await {
                        eprintln!("{:?}",e)
                    }
                });
            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


async fn proxy(mut inbound:TcpStream,mut outbound:TcpStream,cipher:CipherHandler,iv:Vec<u8>)->Res<()>{
    println!("ReadIV = {:?}",iv);

    // send address
    let mut frame = Frame::IPv4(Ipv4Addr::from_str("127.0.0.1")?,22);// ssh address
    frame.write(&mut outbound).await?;

    // swap data
    let capacity = 4 * 1024;
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();
    let mut buffer_inbound = vec![0u8;capacity];
    let mut buffer_outbound = vec![0u8;capacity];

    loop {
        tokio::select! {
                res_inbound = ri.read(&mut buffer_inbound) => match res_inbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        let data = &buffer_inbound[0..sz];
                        let enc = cipher.encrypt_with_iv(data,iv.as_slice())?;
                        wo.write_all(enc.as_slice()).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                },
                res_outbound = ro.read(&mut buffer_outbound) => match res_outbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        let data = &buffer_outbound[0..sz];
                        let dec = cipher.decrypt_with_iv(data,iv.as_slice())?;
                        wi.write_all(dec.as_slice()).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                }
            };
    }
}

