use relay::*;
use tokio::io::AsyncWriteExt;
use tokio::io::AsyncReadExt;


#[tokio::main]
async fn main() -> Res<()> {

    let address = "127.0.0.1:7878";
    let proxy_address = "home.cn:10022";
    let listener = TcpListener::bind(address).await?;
    loop {
        let (inbound,addr) = listener.accept().await?;
        println!("new client: {:?}", addr);
        let outbound = TcpStream::connect(proxy_address).await?;
        let _ = tokio::spawn(async move{
            if let Err(e) = pack(inbound,outbound).await {
                eprintln!("{:?}",e)
            }
        });
    }
}


async fn pack(mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();

    let buffer_size = 4 * 1024;

    // swap
    let _res = tokio::try_join!(async move{
        let mut buf = vec![0u8;buffer_size];
        loop{
            let sz = match ri.read(&mut buf).await {
                Ok(res) if res == 0 => { return Ok(()); },
                Ok(res) => res,
                Err(e) => { return Err(e); }
            };

            println!("Inbound to Outbound = {:?}",&buf[0..sz]);
            wo.write_all(&buf[0..sz]).await?;
        };
        wo.shutdown().await?;
        Ok(())
    },async move{
        let mut buf = vec![0u8;buffer_size];
        loop{
            let sz = match ro.read(&mut buf).await {
                Ok(res) if res == 0 => { return Ok(()); },
                Ok(res) => res,
                Err(e) => { return Err(e); }
            };

            println!("Outbound to Inbound = {:?}",&buf[0..sz]);
            wi.write_all(&buf[0..sz]).await?;
        };
        wi.shutdown().await?;
        Ok(())
    })?;
    Ok(())
}

