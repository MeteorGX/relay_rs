use relay::*;


#[tokio::main]
async fn main() -> Res<()> {
    // configure
    let address = "127.0.0.1:10022";

    // connect tcp listener
    let mut stream = TcpStream::connect(address).await?;


    // command[address]
    let mut contents = String::new();
    println!("Input Address:");
    std::io::stdin().read_line(&mut contents)?;
    let address = contents.trim().to_string();

    // command[port]
    contents.clear();
    println!("Input Port:");
    std::io::stdin().read_line(&mut contents)?;
    let port:u16 = contents.trim().parse()?;

    let (mut ri,mut wo) = stream.split();
    Frame::Hostname(String::from(address),port).write(&mut wo).await?;
    let frame = Frame::read(&mut ri).await?;
    println!("Readable = {:?}",frame);


    Ok(())
}
