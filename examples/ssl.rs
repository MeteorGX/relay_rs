use relay::*;

#[tokio::main]
async fn main() -> Res<()> {

    let method  = "rc4";
    let password = "relay123456";
    let message = "hello.world!";

    let cipher = CipherHandler::new(method,password)?;
    let iv = cipher.generate_iv();

    let encrypt = cipher.encrypt_with_iv(message.as_bytes(),iv.as_slice())?;
    println!("Encrypt = {:?}",encrypt.as_slice());

    let decrypt = cipher.decrypt_with_iv(encrypt.as_slice(),iv.as_slice())?;
    println!("Decrypt = {}",std::str::from_utf8(decrypt.as_slice())?);

    Ok(())
}