use relay::*;
use std::net::{SocketAddr, IpAddr};

#[tokio::main]
async fn main() -> Res<()> {
    // configure
    let address = "127.0.0.1:20022";

    // network
    let listener = TcpListener::bind(address).await?;

    loop {
        match listener.accept().await {
            Ok((inbound, addr)) => {
                println!("new client: {:?}", addr);
                tokio::spawn(async move{
                    if let Err(e) = proxy(inbound).await {
                        eprintln!("{:?}",e)
                    }
                });

            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


async fn proxy(mut inbound:TcpStream)->Res<()>{
    let outbound = match Frame::read(&mut inbound).await? {
        Some(Frame::IPv4(address,port)) => {
            TcpStream::connect(SocketAddr::new(IpAddr::from(address),port)).await?
        },
        Some(Frame::IPv6(address,port)) =>{
            TcpStream::connect(SocketAddr::new(IpAddr::from(address),port)).await?
        },
        Some(Frame::Hostname(address,port))=>{
            TcpStream::connect(format!("{}:{}",address,port)).await?
        },
        _ => {
            return Err(format!("failed by address frame").into())
        }
    };

    CopyStream::from_stream(inbound,outbound).await?;
    Ok(())
}
