use relay::*;

#[tokio::main]
async fn main() -> Res<()> {

    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:7878";

    // network
    let listener = TcpListener::bind_with_cipher(address,Some(cipher)).await?;

    match listener.accept().await {
        Ok((mut socket, addr)) => {
            println!("new client: {:?}", addr);

            let mut buffer = vec![0u8;1024];
            let sz = socket.read(buffer.as_mut_slice()).await?;
            println!("message[{}] = {}",sz,std::str::from_utf8(&buffer.as_slice()[0..sz])?);
        },
        Err(e) => eprintln!("couldn't get client: {:?}", e),
    }

    Ok(())
}
