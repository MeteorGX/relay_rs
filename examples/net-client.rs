use relay::*;

#[tokio::main]
async fn main() -> Res<()> {

    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:7878";

    // network
    let mut stream = TcpStream::connect_with_cipher(address,Some(cipher)).await?;
    stream.write_all(b"hello,world!").await?;


    Ok(())
}
