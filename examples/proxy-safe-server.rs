use relay::*;
use std::net::{SocketAddr, IpAddr};
use tokio::io::{AsyncWriteExt};
use tokio::io::AsyncReadExt;



#[tokio::main]
async fn main() -> Res<()> {
    // configure
    let method  = "chacha20_poly1305";
    let password = "relay123456";
    let cipher = CipherHandler::new(method,password)?;
    let address = "127.0.0.1:20022";



    // network
    let listener = TcpListener::bind(address).await?;
    loop {
        match listener.accept().await {
            Ok((mut inbound, addr)) => {

                // write iv
                let proxy_other = cipher.clone();
                let iv = match proxy_other.write_iv(&mut inbound).await{
                    Ok(res) => res,
                    Err(e) => {
                        eprint!("{:?}",e);
                        return Err(e);
                    }
                };
                println!("new client: {:?}", addr);

                tokio::spawn(async move{
                    if let Err(e) = proxy(inbound,proxy_other,iv).await {
                        eprintln!("{:?}",e)
                    }
                });
            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}



async fn proxy(mut inbound:TcpStream,cipher:CipherHandler,iv:Vec<u8>)->Res<()>{
    println!("WriteIV = {:?}",iv);

    // read address
    let mut outbound = match Frame::read(&mut inbound).await? {
        Some(Frame::IPv4(address,port)) => {
            TcpStream::connect(SocketAddr::new(IpAddr::from(address),port)).await?
        },
        Some(Frame::IPv6(address,port)) =>{
            TcpStream::connect(SocketAddr::new(IpAddr::from(address),port)).await?
        },
        Some(Frame::Hostname(address,port))=>{
            TcpStream::connect(format!("{}:{}",address,port)).await?
        },
        _ => {
            return Err(format!("failed by address frame").into())
        }
    };
    println!("Outbound = {:?}",outbound);


    let capacity = 4 * 1024;
    let (mut ri,mut wi) = inbound.split();
    let (mut ro,mut wo) = outbound.split();
    let mut buffer_inbound = vec![0u8;capacity];
    let mut buffer_outbound = vec![0u8;capacity];

    loop {
        tokio::select! {
                res_inbound = ri.read(&mut buffer_inbound) => match res_inbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        let data = &buffer_inbound[0..sz];
                        let dec = cipher.decrypt_with_iv(data,iv.as_slice())?;
                        wo.write_all(dec.as_slice()).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                },
                res_outbound = ro.read(&mut buffer_outbound) => match res_outbound {
                    Ok(sz) if sz == 0 => { return Ok(()) },
                    Ok(sz) => {
                        let data = &buffer_outbound[0..sz];
                        let enc = cipher.encrypt_with_iv(data,iv.as_slice())?;
                        wi.write_all(enc.as_slice()).await?;
                    },
                    Err(e) => { return Err(e.into()) }
                }
            };
    }
}

