use relay::*;

#[tokio::main]
async fn main() -> Res<()> {

    // configure[ssh proxy]
    let address = "127.0.0.1:10022";
    let proxy_address = "127.0.0.1:22";

    // network
    let listener = TcpListener::bind(address).await?;

    loop {
        match listener.accept().await {
            Ok((inbound, addr)) => {
                println!("new client: {:?}", addr);

                let outbound= match TcpStream::connect(proxy_address).await{
                    Ok(res) => res,
                    Err(e) => {
                        eprintln!("{:?}",e);
                        continue;
                    }
                };

                tokio::spawn(async move{
                    if let Err(e) = proxy(inbound,outbound).await {
                        eprintln!("{:?}",e)
                    }
                });

            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


async fn proxy(mut inbound:TcpStream,mut outbound:TcpStream)->Res<()>{
    inbound.copy(&mut outbound).await?;
    Ok(())
}
