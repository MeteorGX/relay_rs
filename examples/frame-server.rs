use relay::*;


#[tokio::main]
async fn main() -> Res<()> {
    // bind tcp listener
    let address = "127.0.0.1:10022";
    let listener = TcpListener::bind(address).await?;
    if let Err(e) = run(listener).await{
        eprintln!("{:?}",e);
    };
    Ok(())
}


pub async fn run(listener:TcpListener)->Res<()>{
    loop {
        match listener.accept().await {
            Ok((socket, addr)) => {
                println!("new client: {:?}", addr);
                tokio::spawn(async move {
                    if let Err(e) = read_frame(socket).await{
                        eprintln!("{:?}",e);
                    }
                });
            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }
}


pub async fn read_frame(mut inbound:TcpStream)->Res<()>{
    let (mut ri,mut wi) = inbound.split();
    let frame = Frame::read(&mut ri).await?;
    println!("Frame = {:?}",frame);
    if let Some(mut f) = frame {
        f.write(&mut wi).await?;
    }

    Ok(())
}