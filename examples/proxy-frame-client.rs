use relay::*;
use std::net::Ipv4Addr;
use std::str::FromStr;


#[tokio::main]
async fn main() -> Res<()> {
    // command[address]
    let mut contents = String::new();
    println!("Input Address:");
    std::io::stdin().read_line(&mut contents)?;
    let input_address = contents.trim().to_string();

    // command[port]
    contents.clear();
    println!("Input Port:");
    std::io::stdin().read_line(&mut contents)?;
    let input_port:u16 = contents.trim().parse()?;


    // configure
    let address = "127.0.0.1:10022";
    let proxy_address = "127.0.0.1:20022";

    // network
    let listener = TcpListener::bind(address).await?;

    loop {
        match listener.accept().await {
            Ok((inbound, addr)) => {
                println!("new client: {:?}", addr);

                let outbound = TcpStream::connect(proxy_address).await?;
                let address_frame = Frame::IPv4(Ipv4Addr::from_str(input_address.as_str())?,input_port);
                tokio::spawn(async move{
                    if let Err(e) = proxy(inbound,outbound,address_frame).await {
                        eprintln!("{:?}",e)
                    }
                });

            },
            Err(e) => eprintln!("couldn't get client: {:?}", e),
        }
    }

}

async fn proxy(inbound:TcpStream,mut outbound:TcpStream,mut frame:Frame)->Res<()>{
    frame.write(&mut outbound).await?;
    CopyStream::from_stream(inbound,outbound).await?;
    Ok(())
}